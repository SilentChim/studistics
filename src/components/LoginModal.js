import React, { useState } from "react";
import { Button, ButtonToolbar, Modal } from "react-bootstrap";
import Login from "../containers/Login";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Welcome back!
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Login/>
      </Modal.Body>
    </Modal>
  );
}

export default function LoginModal() {
  const [modalShow, setModalShow] = React.useState(false);

  return (
    <ButtonToolbar>
      <Button variant="primary" onClick={() => setModalShow(true)}>
        Login
      </Button>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </ButtonToolbar>
  );
}