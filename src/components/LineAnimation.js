import Snap from 'snapsvg-cjs';
import "../../styles.scss";

const weight = [0,-95, -30, -65, -35, -155, -85, -60, -215];
//CHART VALUES

let chartH = $('#svg').height();
let chartW = $('#svg').width();

// PARSE PRICES TO ALIGN WITH BOTTOM OF OUR SVG

let weights = [];
for (let i = 0; i < weight.length; i++) {
  weights[i] = weight[i] + $('#svg').height();
}

function draw() {
  //DEFINE SNAP SVG AND DETERMINE STEP NO.
  let paper = Snap('#svg');
  let steps = weights.length;

  // EVENLY DISTRIBUTE OUR POINTS ALONG THE X AXIS

  function step(i, chartW) {
    return chartW / weights.length * i;
  }

  let points = [];
  // let breakPointsX = [];
  // let breakPointsY = [];
  let point = {};

  for (let i = 1; i < weights.length; i++) {

    //CALCULATE CURRENT POINT

    let currStep = step(i, chartW);
    let y = weights[i];
    point.x = Math.floor(currStep);
    point.y = y;

    //CALCULATE PREVIOUS POINT

    let prev = i - 1;
    let prevStep = step(prev, chartW);
    let prevY = weights[prev];
    point.prevX = Math.floor(prevStep);
    point.prevY = prevY;
    if (point.prevX === 0 || point.prevY === 0) {
      point.prevX = 15;
      point.prevY = chartH - 15;
    }
    // SAVE PATH TO ARRAY
    points[i] = " M" + point.prevX + "," + point.prevY + " L" + point.x + "," + point.y;

    // SAVE BREAKPOINTS POSITION

    let r = 30;
    // breakPointsX[i] = point.x;
    // breakPointsY[i] = point.y;
  }

  // DRAW LINES

  for (let i = 0; i < points.length; i++) {
    let myPath = paper.path(points[i]);
    let len = myPath.getTotalLength();
    myPath.attr({
      'stroke-dasharray': len,
      'stroke-dashoffset': len,
      'stroke': 'white',
      'stroke-linecap': 'round',
      'stroke-width': 4,
      'stroke-linejoin': 'round',
      'id': 'myLine' + i,
      'class': 'line'
    });
  }

  // DRAW COORDINATE SYSTEM

  let xAxis = paper.path('M0,'+chartH+'L'+chartW+","+chartH);
  let yAxis = paper.path('M0,'+chartH+'L0,0');

  let xOff = xAxis.getTotalLength();
  let yOff = yAxis.getTotalLength();
  let start = (weights.length*250+"ms");

  yAxis.attr({
    'stroke':'white',
    'stroke-width':1,
    'stroke-dasharray':yOff,
    'stroke-dashoffset':yOff,
    'id':'yAxis'
  });
  xAxis.attr({
    'stroke':'white',
    'stroke-width':1,
    'stroke-dasharray':xOff,
    'stroke-dashoffset':xOff,
    'id':'xAxis'
  });
  console.log(start);
  $('#yAxis').css({
    '-webkit-transition-delay':start,
    '-webkit-transition':'all 200ms ease-in'
  });
  $('#xAxis').css({
    '-webkit-transition-delay':start,
    '-webkit-transition':'all 200ms ease-in'
  });
  $('#xAxis').animate({
    'stroke-dashoffset':'0'
  });
  $('#yAxis').animate({
    'stroke-dashoffset': '0'
  });
}
function animate(){
  for (let i=0; i<weights.length; i++){
    let circ = $('#myCirc'+i);
    let line = $('#myLine'+i);
    circ.css({
      '-webkit-transition':'all 550ms cubic-bezier(.84,0,.2,1)',
      '-webkit-transition-delay':375+(i*125)+"ms"
    });
    line.css({
      '-webkit-transition':'all 250ms cubic-bezier(.84,0,.2,1)',
      '-webkit-transition-delay':i*125+"ms"
    });
    line.animate({
      'stroke-dashoffset':0
    });
    circ.css({
      'transform':'scale(1)'
    });
  }
}
$(window).load(function(){
  draw();
  animate();
})
$('#draw').on('click',function(){
  $('#svg').empty();
  draw();
  animate();
});
